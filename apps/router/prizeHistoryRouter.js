const express = require("express");

const router = express.Router();

const {createPrizeHistory, getAllPrizeHistory, getPrizeHistoryById,updatePrizeHistoryById, deletePrizeHistoryById, getPrizeHistoryByUserName } = require('../controllers/prizeHistoryController');


router.post('/prize-histories', createPrizeHistory);
router.get('/prize-histories', getAllPrizeHistory);
router.get('/prize-histories/:historyId', getPrizeHistoryById );

router.get('/devcamp-lucky-dice/prize-history', getPrizeHistoryByUserName );

router.put('/prize-histories/:historyId', updatePrizeHistoryById );
router.delete('/prize-histories/:historyId', deletePrizeHistoryById );


module.exports = router;