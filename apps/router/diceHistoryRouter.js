const express = require("express");


const router = express.Router();

const {createDiceHistory, getAllDiceHistory, getDiceHistoryById, updateDiceHistoryById, deleteDiceHistoryById, getDiceHistoryByUserName} = require('../controllers/diceHistoryController');



router.post('/dice-histories', createDiceHistory);
router.get('/dice-histories', getAllDiceHistory);
router.get('/dice-histories/:diceHistoryId', getDiceHistoryById);

router.get('/devcamp-lucky-dice/dice-history', getDiceHistoryByUserName);

router.put('/dice-histories/:diceHistoryId', updateDiceHistoryById);
router.delete('/dice-histories/:diceHistoryId', deleteDiceHistoryById);


module.exports = router;