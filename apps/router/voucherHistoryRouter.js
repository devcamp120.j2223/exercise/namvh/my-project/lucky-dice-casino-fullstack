const express = require("express");

const router = express.Router();

const {createVoucherHistory , getAllVoucherHistory , getVoucherHistoryById ,updateVoucherHistoryById , deleteVoucherHistoryById, getVoucherHistoryByUserName} = require('../controllers/voucherHistoryController');


router.post('/voucher-histories', createVoucherHistory );
router.get('/voucher-histories', getAllVoucherHistory );
router.get('/voucher-histories/:historyId', getVoucherHistoryById  );

router.get('/devcamp-lucky-dice/voucher-history', getVoucherHistoryByUserName  );

router.put('/voucher-histories/:historyId', updateVoucherHistoryById  );
router.delete('/voucher-histories/:historyId', deleteVoucherHistoryById  );


module.exports = router;