const prizeModel = require('../model/prizeModel');

const mongoose  = require("mongoose");

const createPrize = (req, res) => {
    //B1:
    let bodyReq =  req.body;
    //B2
    if(!bodyReq.name){
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "name in invalid"
        })
    }
    //B3 
    let createBody = {
        _id: mongoose.Types.ObjectId(),
        name: bodyReq.name,
        description: bodyReq.description
    }
    prizeModel.create(createBody, (err, data) => {
        if(err){
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(201).json({
                status: "Success: Create Prize success!!!",
                data: data
            })
        }
    })
}

const getAllPrize = (req, res) => {
    //B1:
    //B2:
    //B3:
    prizeModel.find((err , data) => {
        if(err){
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Success: Get All Prize success!!!",
                data: data
            })
        }
    })
}

const getPrizeById = (req, res) => {
    //B1;
    let prizeId = req.params.prizeId;
    //B2:
    if(!mongoose.Types.ObjectId.isValid(prizeId)){
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "prizeId in invalid"
        }) 
    }
    //B3:
    prizeModel.findById(prizeId, (err, data) => {
        if(err){
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Success: Get Prize By Id success!!!",
                data: data
            })
        }
    })
}

const updatePrizeById = (req, res) => {
    //B1: 
    let prizeId = req.params.prizeId;
    let bodyReq = req.body;
    //B2:
    if(!mongoose.Types.ObjectId.isValid(prizeId)){
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "prizeId in invalid"
        }) 
    }else{
    //B3
    let updateBody = {
        name: bodyReq.name,
        description: bodyReq.description
    }    
    prizeModel.findByIdAndUpdate(prizeId, updateBody, (err, data) => {
        if(err){
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Success: Update Prize By Id success!!!",
                data: data
            })
        }
    })
    }
}

const deletePrizeById = (req, res) => {
    //B1: 
    let prizeId = req.params.prizeId;
    //B2:
    if(!mongoose.Types.ObjectId.isValid(prizeId)){
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "prizeId in invalid"
        }) 
    }
    //B3
    prizeModel.findByIdAndDelete(prizeId, (err) => {
        if(err){
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(204).json({
                status: "Success: Delete Dice By Id success!!!",
            })
        }
    })
}


module.exports = { createPrize, getAllPrize,  getPrizeById, updatePrizeById,deletePrizeById}