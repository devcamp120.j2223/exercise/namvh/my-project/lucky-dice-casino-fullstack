const userModel = require('../model/userModel');

const mongoose = require("mongoose");
const { restart } = require('nodemon');


const createUser = (req, res) => {
    //B1:
    let bodyReq = req.body;
    //B2:
    if(!bodyReq.username || !bodyReq.firstname || !bodyReq.lastname ){
        return res.status(400).json({
            status: 'Error 400: Bad request',
            message: 'data in required value'
        })
    }else{
     //B3:
     let createBody = {
        _id: mongoose.Types.ObjectId(),
        username: bodyReq.username,
        firstname: bodyReq.firstname,
        lastname: bodyReq.lastname
     }   
     userModel.create(createBody, (err, data) => {
        if(err){
            return res.status(500).json({
                status: 'Error 500: Internal server error',
                message: err.message
            })
        }else{
            res.status(201).json({
                status: 'Success: Create User success!!!',
                data: data
            })
        }
     })

    }
}

const getAllUser = (req,res) => {
    //B1
    //B2
    //B3
    userModel.find((err, data) => {
        if(err){
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Success: GetAllUser success!!!",
                data: data
            })
        }
    })
}

const getUserById = (req, res) => {
    //B1: 
    let userId = req.params.userId;
    //B2:
    if(!mongoose.Types.ObjectId.isValid(userId)){
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: err.message
        })
    }
    //B3
    userModel.findById(userId, (err, data) => {
        if(err){
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Success: getUserById success!!!",
                data: data
            })
        }
    })
        
    
}

const updateUserById = (req, res) => {
    //B1:
    let userId = req.params.userId;
    let bodyReq = req.body;
    //B2:
    if(!mongoose.Types.ObjectId.isValid(userId)){
        return res.status(400).json({
            status: 'Error 400: Bad request',
            message: "userId is in valid"
        })
    }
    //B3:
    let bodyUpdate = {
        username: bodyReq.username,
        firstname: bodyReq.firstname,
        lastname: bodyReq.lastname
    }
    userModel.findByIdAndUpdate(userId, bodyUpdate, (err, data) => {
        if(err){
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Success: UpdataInfo success!!",
                data: data
            })
        }
    })
}

const deleteUserById = (req, res) =>{
    let userId = req.params.userId;
    //B2: validate
    if(!mongoose.Types.ObjectId.isValid(userId)){
        res.status(400).json({
            status: 'Error 400: Bad request',
            message: "userId is invalid"
        })
    }
    //B3 thao tac CSDL
    userModel.findByIdAndDelete(userId, (err, data) => {
        if(err){
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(204).json({
                status: "Success: Delete course success!!!",
            })
        }
    })
} 

module.exports = { createUser, getAllUser, getUserById, updateUserById , deleteUserById};