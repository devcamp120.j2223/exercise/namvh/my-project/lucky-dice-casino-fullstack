const voucherHistoryModel = require('../model/voucherHistoryModel');
const userModel = require('../model/userModel');

const mongoose  = require("mongoose");

const createVoucherHistory = (req, res) => {
    //B1:
    let bodyReq =  req.body;
    //B2
    if(!mongoose.Types.ObjectId.isValid(bodyReq.user)){
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "User is invalid"
        })
    }
    if(!mongoose.Types.ObjectId.isValid(bodyReq.voucher)){
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "voucher is invalid"
        })
    }
    //B3:
    let createBody = {
        _id: mongoose.Types.ObjectId(),
        user: bodyReq.user,
        voucher: bodyReq.voucher
    }
   
    voucherHistoryModel.create(createBody, (err, data) => {
        if(err){
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(201).json({
                status: "Success: Create voucherHistory success!!!",
                data: data
            })
        }
    })

    
}

const getAllVoucherHistory = (req, res) => {
    //B1:
        
    let user = req.query.user
    
    let condition = {};

    if(user){
        condition.user = user
    }

    console.log(condition);

    //B2:
    //B3:
    voucherHistoryModel.find(condition, (err , data) => {
        if(err){
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Success: Get All Voucher success!!!",
                data: data
            })
        }
    })
}

const getVoucherHistoryById  = (req, res) => {
    //B1;
    let historyId = req.params.historyId;
    //B2:
    if(!mongoose.Types.ObjectId.isValid(historyId)){
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "historyId in invalid"
        }) 
    }
    //B3:
    voucherHistoryModel.findById(historyId, (err, data) => {
        if(err){
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Success: Get VoucherHistory By Id success!!!",
                data: data
            })
        }
    })
}

const updateVoucherHistoryById  = (req, res) => {
    //B1: 
    let historyId = req.params.historyId;
    let bodyReq = req.body;
    //B2:
    if(!mongoose.Types.ObjectId.isValid(bodyReq.user)){
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "User is invalid"
        })
    }
    if(!mongoose.Types.ObjectId.isValid(bodyReq.voucher)){
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "Voucher is invalid"
        })
    }
    if(!mongoose.Types.ObjectId.isValid(historyId)){
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "historyId in invalid"
        }) 
    }else{
    //B3
    let updateBody = {
        user: bodyReq.user,
        voucher: bodyReq.voucher
    }    
    voucherHistoryModel.findByIdAndUpdate(historyId, updateBody, (err, data) => {
        if(err){
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Success: Update VoucherHistory By Id success!!!",
                data: data
            })
        }
    })
    }
}

const deleteVoucherHistoryById = (req, res) => {
    //B1: 
    let historyId = req.params.historyId;
    //B2:
    if(!mongoose.Types.ObjectId.isValid(historyId)){
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "historyId in invalid"
        }) 
    }
    //B3
    voucherHistoryModel.findByIdAndDelete(historyId, (err) => {
        if(err){
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(204).json({
                status: "Success: Delete Voucher By Id success!!!",
            })
        }
    })
}


const getVoucherHistoryByUserName = (req, res) => {
    //B1:
    let username = req.query.username;
    //B2:
    if(!username) {
        voucherHistoryModel.find( (err, data) => {
            if(err) {
                return res.status(500).json({
                    status: "Error 500: Internal server error",
                    message: err.message
                })
            }
        })
    } else{
        userModel.findOne( {username}, (errFind, dataUser) => {
            if(errFind) {
                return res.status(500).json({
                    status: "Error 500: Internal server error",
                    message: errFind.message
                })
            }else {
                if(!dataUser) {
                    res.status(500).json({
                        status: "Error 500: Internal server error",
                        message: `DataUser is not found` + []
                    })
                }else {
                    voucherHistoryModel.find( {user: dataUser._id}, (err, data) =>{
                        if(err) {
                            return res.status(500).json({
                                status: "Error 500: Internal server error",
                                message: err.message
                            })
                        }else {
                            return res.status(200).json({
                                voucher: data,
                                status: `200`
                            })
                        }
                    }).populate('voucher')
                }
            }
        })
    }
}

module.exports = {createVoucherHistory, getAllVoucherHistory, getVoucherHistoryById, updateVoucherHistoryById, deleteVoucherHistoryById, getVoucherHistoryByUserName}