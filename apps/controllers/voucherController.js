const voucherModel = require('../model/voucherModel');

const mongoose  = require("mongoose");

const createVoucher = (req, res) => {
    //B1:
    let bodyReq =  req.body;
    //B2
    if(!bodyReq.code){
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "code in invalid"
        })
    }
    if(!(Number.isInteger(bodyReq.discount) && bodyReq.discount > 0)){
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "discount in invalid"
        })
    }
    //B3 
    let createBody = {
        _id: mongoose.Types.ObjectId(),
        code: bodyReq.code,
        discount: bodyReq.discount,
        note: bodyReq.note
    }
    voucherModel.create(createBody, (err, data) => {
        if(err){
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(201).json({
                status: "Success: Create Voucher success!!!",
                data: data
            })
        }
    })
}

const getAllVoucher = (req, res) => {
    //B1:
    //B2:
    //B3:
    voucherModel.find((err , data) => {
        if(err){
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Success: Get All voucher success!!!",
                data: data
            })
        }
    })
}

const getVoucherById = (req, res) => {
    //B1;
    let voucherId = req.params.voucherId;
    //B2:
    if(!mongoose.Types.ObjectId.isValid(voucherId)){
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "voucherId in invalid"
        }) 
    }
    //B3:
    voucherModel.findById(voucherId, (err, data) => {
        if(err){
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Success: Get Voucher By Id success!!!",
                data: data
            })
        }
    })
}

const updateVoucherById = (req, res) => {
    //B1: 
    let voucherId = req.params.voucherId;
    let bodyReq = req.body;
    //B2:
    if(!mongoose.Types.ObjectId.isValid(voucherId)){
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "voucherId in invalid"
        }) 
    }
    if(!(Number.isInteger(bodyReq.discount) && bodyReq.discount > 0)){
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "Discount is a number"
        }) 
    }
    else{
    //B3
    let updateBody = {
        code: bodyReq.code,
        discount: bodyReq.discount,
        note: bodyReq.note
    }    
    voucherModel.findByIdAndUpdate(voucherId, updateBody, (err, data) => {
        if(err){
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Success: Update Voucher By Id success!!!",
                data: data
            })
        }
    })
    }
}

const deleteVoucherById = (req, res) => {
    //B1: 
    let voucherId = req.params.voucherId;
    //B2:
    if(!mongoose.Types.ObjectId.isValid(voucherId)){
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "voucherId in invalid"
        }) 
    }
    //B3
    voucherModel.findByIdAndDelete(voucherId, (err) => {
        if(err){
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(204).json({
                status: "Success: Delete Voucher By Id success!!!",
            })
        }
    })
}


module.exports = { createVoucher, getAllVoucher, getVoucherById, updateVoucherById, deleteVoucherById}