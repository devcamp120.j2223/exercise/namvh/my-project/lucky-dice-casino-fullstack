const prizeHistoryModel = require('../model/prizeHistoryModel');
const userModel = require('../model/userModel');

const mongoose  = require("mongoose");

const createPrizeHistory = (req, res) => {
    //B1:
    let bodyReq =  req.body;
    //B2
    if(!mongoose.Types.ObjectId.isValid(bodyReq.user)){
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "User is invalid"
        })
    }
    if(!mongoose.Types.ObjectId.isValid(bodyReq.prize)){
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "Prize is invalid"
        })
    }
    //B3:
    let createBody = {
        _id: mongoose.Types.ObjectId(),
        user: bodyReq.user,
        prize: bodyReq.prize
    }
   
    prizeHistoryModel.create(createBody, (err, data) => {
        if(err){
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(201).json({
                status: "Success: Create prizeHistory success!!!",
                data: data
            })
        }
    })

    
}

const getAllPrizeHistory = (req, res) => {
    //B1:
    let user = req.query.user;
    let condition = {};

    if(user){
        condition.user = user
    }

    console.log(condition);

    //B2:
    //B3:
    prizeHistoryModel.find(condition, (err , data) => {
        if(err){
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Success: Get All Prize success!!!",
                data: data
            })
        }
    })
}

const getPrizeHistoryById  = (req, res) => {
    //B1;
    let historyId = req.params.historyId;
    //B2:
    if(!mongoose.Types.ObjectId.isValid(historyId)){
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "historyId in invalid"
        }) 
    }
    //B3:
    prizeHistoryModel.findById(historyId, (err, data) => {
        if(err){
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Success: Get PrizeHistory By Id success!!!",
                data: data
            })
        }
    })
}

const updatePrizeHistoryById  = (req, res) => {
    //B1: 
    let historyId = req.params.historyId;
    let bodyReq = req.body;
    //B2:
    if(!mongoose.Types.ObjectId.isValid(bodyReq.user)){
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "User is invalid"
        })
    }
    if(!mongoose.Types.ObjectId.isValid(bodyReq.prize)){
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "Prize is invalid"
        })
    }
    if(!mongoose.Types.ObjectId.isValid(historyId)){
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "historyId in invalid"
        }) 
    }else{
    //B3
    let updateBody = {
        user: bodyReq.user,
        prize: bodyReq.prize
    }    
    prizeHistoryModel.findByIdAndUpdate(historyId, updateBody, (err, data) => {
        if(err){
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Success: Update PrizeHistory By Id success!!!",
                data: data
            })
        }
    })
    }
}

const deletePrizeHistoryById = (req, res) => {
    //B1: 
    let historyId = req.params.historyId;
    //B2:
    if(!mongoose.Types.ObjectId.isValid(historyId)){
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "historyId in invalid"
        }) 
    }
    //B3
    prizeHistoryModel.findByIdAndDelete(historyId, (err) => {
        if(err){
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(204).json({
                status: "Success: Delete Dice By Id success!!!",
            })
        }
    })
}

const getPrizeHistoryByUserName = (req, res) => {
    //B1
    let username = req.query.username;
    //B2
    if(!username) {
        prizeHistoryModel.find( (err, data) => {
            if(err) {
                return res.status(500).json({
                    status: "Error 500: Internal server error",
                    message: err.message
                })
            }
        })
    } else {
        userModel.findOne( {username}, (errFind, userData) => {
            if(errFind) {
                return res.status(500).json({
                    status: "Error 500: Internal server error",
                    message: err.message
                })
            } else {
                if(!userData) {
                    return res.status(500).json({
                        status: "Error 500: Internal server error",
                        message: err.message
                    })
                } else {
                    prizeHistoryModel.find ( {user: userData._id}, (err,data) => {
                        if(err) {
                            return res.status(500).json({
                                status: "Error 500: Internal server error",
                                message: err.message
                            })
                        } else {
                            return res.status(200).json({
                                prize: data,
                                status: '200'
                            })
                        }
                    }).populate("prize")
                }
            }
        })
    }
}

module.exports = {createPrizeHistory, getAllPrizeHistory, getPrizeHistoryById, updatePrizeHistoryById, deletePrizeHistoryById, getPrizeHistoryByUserName}