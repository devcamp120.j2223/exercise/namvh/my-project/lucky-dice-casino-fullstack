const diceHistoryModel = require('../model/diceHistoryModel');
const userModel = require('../model/userModel');

const mongoose  = require("mongoose");

const createDiceHistory = (req, res) => {
    //B1:
    let bodyReq =  req.body;
    //B2
    if(!bodyReq.dice){
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "dice in invalid"
        })
    }
    if(!mongoose.Types.ObjectId.isValid(bodyReq.user)){
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "UserId in invalid"
        })   
    }
    //B3 
    let createBody = {
        _id: mongoose.Types.ObjectId(),
        user: bodyReq.user,
        dice: bodyReq.dice
    }
    diceHistoryModel.create(createBody, (err, data) => {
        if(err){
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(201).json({
                status: "Success: Create Dice success!!!",
                data: data
            })
        }
    })
}

const getAllDiceHistory = (req, res) => {
    //B1:
    let user = req.query.user;
    let condition = {};

    if(user){
        condition.user = user
    }

    console.log(condition);
    
    //B2:
    //B3:
    diceHistoryModel.find(condition, (err , data) => {
        if(err){
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Success: Get All Dice success!!!",
                data: data,
            })
        }
    })
}

const getDiceHistoryById = (req, res) => {
    //B1;
    let diceHistoryId = req.params.diceHistoryId;
    //B2:
    if(!mongoose.Types.ObjectId.isValid(diceHistoryId)){
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "diceHistoryId in invalid"
        }) 
    }
    //B3:
    diceHistoryModel.findById(diceHistoryId, (err, data) => {
        if(err){
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Success: Get Dice By Id success!!!",
                data: data
            })
        }
    })
}

const updateDiceHistoryById = (req, res) => {
    //B1: 
    let diceHistoryId = req.params.diceHistoryId;
    let bodyReq = req.body;
    //B2:
    if(!mongoose.Types.ObjectId.isValid(diceHistoryId)){
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "diceHistoryId in invalid"
        }) 
    }else{
    //B3
    let updateBody = {
        // user: mongoose.Types.ObjectId(),
        dice: bodyReq.dice
    }    
    diceHistoryModel.findByIdAndUpdate(diceHistoryId, updateBody, (err, data) => {
        if(err){
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Success: Update Dice By Id success!!!",
                data: data
            })
        }
    })
    }
}

const deleteDiceHistoryById = (req, res) => {
    //B1: 
    let diceHistoryId = req.params.diceHistoryId;
    //B2:
    if(!mongoose.Types.ObjectId.isValid(diceHistoryId)){
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "diceHistoryId in invalid"
        }) 
    }
    //B3
    diceHistoryModel.findByIdAndDelete(diceHistoryId, (err) => {
        if(err){
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(204).json({
                status: "Success: Delete Dice By Id success!!!",
            })
        }
    })
}


const getDiceHistoryByUserName = (req, res) => {
    //B1:
    let username = req.query.username;
    //B2:
    if(!username) {
        return diceHistoryModel.find((errFind) =>{
            if(errFind){
                return  res.status(500).json({
                    status: "Error 500: Internal server error",
                    message: errFind.message + 'diceHistory have not existed yet'
                })
            } 
        })
    } else {
        userModel.findOne( {username}, (errUser, dataUser) => {
            if(errUser) {
                return res.status(500).json({
                    status: "Error 500: Internal server error",
                    message: "Usename is not found" + errUser.message
                })
            } else {
                if(!dataUser) {
                    return res.status(500).json({
                        status: "Error 500: Internal server error",
                        message: 'DataUser is not found' + []
                    })
                } else {
                    diceHistoryModel.find( {user: dataUser._id}, (err, data) => {
                        if(err) {
                            return res.status(500).json({
                                status: "Error 500: Internal server error",
                                message: err.message
                            })
                        }else {
                            return res.status(200).json({
                                dice: data,
                               status: "200"
                            })
                        }
                    })
                }
            }
        })
    }

}

module.exports = { createDiceHistory, getAllDiceHistory,  getDiceHistoryById, updateDiceHistoryById,deleteDiceHistoryById, getDiceHistoryByUserName}