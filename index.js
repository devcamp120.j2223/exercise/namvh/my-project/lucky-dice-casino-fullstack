const express = require("express");

const mongoose = require("mongoose");

const app = express();

const port = 8000;

const path = require("path");

app.get('/', (req, res) => {
    console.log(__dirname);
    res.sendFile(path.join(__dirname + '/views/Lucky-Dice.html'))
})

app.use(express.static(__dirname + '/views'));


const userRouter = require('./apps/router/userRouter');
const diceHistoryRouter = require('./apps/router/diceHistoryRouter');
const prizeRouter = require('./apps/router/prizeRouter');
const voucherRouter = require('./apps/router/voucherRouter');
const prizeHistoryRouter = require('./apps/router/prizeHistoryRouter');
const voucherHistoryRouter = require('./apps/router/voucherHistoryRouter');
const diceRouter = require('./apps/router/diceRouter');



// cau hinh de app doc dk du lieu dang body.json
app.use(express.json());

//cau hinh de ho tro tieng viet
app.use(express.urlencoded({
    extended: true
}))

mongoose.connect('mongodb://localhost:27017/Lucky_Dice_Casino', (err) => {
    if(err){
        throw err;
    }
    console.log(`Connect mongoDB successfully!!!`);
})

const {userSchema } = require ('./apps/model/userModel');
// const { MongoKerberosError } = require("mongodb");

app.use((req, res, next) => {
    console.log('TIme: ', new Date());
    console.log('Request Method: ', req.method);

    next();
}),


app.get('/random-number', (req, res) => {
    
    let randomNumber = Math.floor(Math.random() * 6+1);
    
    res.status(200).json({
        randomNumber,
    })
})


app.use('/', userRouter);
app.use('/', diceHistoryRouter);
app.use('/', prizeRouter);
app.use('/', voucherRouter);
app.use('/', prizeHistoryRouter);
app.use('/', voucherHistoryRouter);
app.use('/', diceRouter);


app.listen(port, () => {
    console.log(`App is running ${port}`);
})